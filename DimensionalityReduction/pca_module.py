#
# N. Uras - 07/10/2019
# Dimensionality reduction first script.
# In this file we implement a first technique of dimensionality reduction, 
# that is Principal Component Analysis (PCA).
#


import os
import joblib
this_dir, _ = os.path.split(__file__)
from sklearn.decomposition import PCA
pca_path = os.path.join(this_dir, 'fitted_pca')


class PCAClass():
    
    
    def __init__(self, X_train, X_test):
        self.X_train = X_train
        self.X_test = X_test
        
        
    def compute_pca(self, explained_variance=0.95):
        # Notice that .95 means that scikit-learn choose the minimum number of 
        # principal components such that 95% of the variance is retained.
        # creating PCA instance
        pca_scores = []
        pca = PCA(explained_variance)
        X_train_fitted = pca.fit_transform(self.X_train)
        joblib.dump(pca, os.path.join(pca_path, 'pca.pkl'))
        X_test_transf = pca.transform(self.X_test)
        pca_scores.append(pca.n_components_)
        print('PCA generated components: {}'.format(pca.n_components_))
        pca_scores.append(pca.explained_variance_ratio_)
        
        return X_train_fitted, X_test_transf, pca_scores
