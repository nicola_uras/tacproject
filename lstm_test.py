#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 27 17:39:29 2020

@author: nicolauras
"""

import numpy as np
import pandas as pd
from keras.utils import np_utils
from tensorflow.keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM 
from tensorflow.keras.callbacks import EarlyStopping

# load dataset
crypto = 'btc'
df = pd.read_csv(f'data/{crypto}_hourly_stock_affect_data.csv')

df = df.iloc[:15000, :5]

# creating target variable
df['diff'] = df['close'] - df['open']
df['class'] = 1
df.loc[df['diff'] <= 0, 'class'] = 0
df.drop('diff', inplace=True, axis=1)

# creating temporal learning structure - con la differenza di prezzo al tempo t 
# voglio prevedere il movimento del prezzo al tempo t+1
# Quindi dobbiamo shiftare le label verso l'alto e mi troverò un nan nell'ultima riga
# del dataset, per cui quella riga verrà eliminata
def temporal_structure(dataset, n_lag, n_forecast, class_name):
    X, y = pd.DataFrame(), pd.DataFrame()
    # creating output
    for i in range(0, n_forecast):
        if i == 0:
            y[class_name] = dataset[class_name].shift(-i)
        else:
            y[f'{class_name}_t+{i}'] = dataset[class_name].shift(-i)
    # creating inputs
    for col in dataset.columns:
        for i in range(n_lag, 0, -1):
            X[f'{col}_t-{i}'] = dataset[col].shift(i)
    # aggregating inputs and outputs
    df_agg = pd.concat([X, y], axis=1)
    # dropping nans
    df_agg.dropna(inplace=True)
    # resetting indeces
    df_agg.reset_index(inplace=True, drop=True)
    
    return df_agg

# testing temporal structure
df_time_series = temporal_structure(df, n_lag=1, n_forecast=1, class_name='class')

# creating inputs and output
X, y = df_time_series.iloc[:, :-1].values, df_time_series['class'].values

# split into train and test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# scale data
scaler = MinMaxScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

# encoding outputs
# label encoding
label_encoder = LabelEncoder()
y_train = label_encoder.fit_transform(y_train)
y_test = label_encoder.transform(y_test)
# one hot encoding
y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)

# reshape X as lstm expected inputs
X_train_reshaped = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
X_test_reshaped = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

# build lstm model
model = Sequential()
model.add(LSTM(64, return_sequences=False, input_shape=(X_test_reshaped.shape[1], 1), activation='relu'))
model.add(Dense(1, activation='sigmoid'))
optimizer = Adam(learning_rate=1e-4)
model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
model.summary()
early_stop = EarlyStopping(monitor='val_loss', mode='min', patience=40)
model.fit(X_train_reshaped, y_train, batch_size=64, epochs=200, 
          validation_data=(X_test_reshaped, y_test), verbose=0, callbacks=[early_stop])

# build CNN model
from tensorflow.keras.layers import Conv1D, Dense
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.layers import LeakyReLU
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Dropout, Flatten

model = Sequential()
model.add(Conv1D(input_shape=(X_train.shape[1], 1), 
                 filters=16, 
                 kernel_size=4, 
                 padding='same',
                 activation='relu'))
model.add(BatchNormalization())
model.add(LeakyReLU())
model.add(Dropout(0.5))
model.add(Conv1D(filters=8, 
                 kernel_size=4, 
                 padding='same', 
                 activation='relu'))
model.add(BatchNormalization())
model.add(LeakyReLU())
model.add(Dropout(0.5))
model.add(Flatten())
model.add(Dense(64))
model.add(BatchNormalization())
model.add(LeakyReLU())
model.add(Dense(1, activation='sigmoid'))
# compiling the model
# multiclass classification: softmax as activation and categorical_crossentropy as loss
# multilabel classification: sigmoid as activation and binary_crossentropy (or Sigmoid Cross-Entropy) as loss
model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
model.fit(X_train_reshaped, y_train, batch_size=64, epochs=200, 
          validation_data=(X_test_reshaped, y_test), verbose=0, callbacks=[early_stop])

# checking model performance
metrics = pd.DataFrame(model.history.history)
metrics[['loss', 'val_loss']].plot()
metrics[['acc', 'val_acc']].plot()

# making predictions
y_preds = model.predict(X_test_reshaped)

# from one hot encoded to 1d array
y_test_1d = np.argmax(y_test, axis=-1)
y_preds_1d = np.argmax(y_preds, axis=-1)

from sklearn.metrics import classification_report
y_preds = y_preds.round()
print(classification_report(y_test, y_preds))
