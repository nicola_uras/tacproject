#
# N. Uras - 03/10/2019
# First script of the project. 
# This file creates the dataframe and store it in mysql server database.
#

import os
import json
import talib as ta
import pandas as pd
from sqlalchemy import create_engine
this_dir, _ = os.path.split(__file__)

# opening btc data
btc = pd.read_csv('data/btc_hourly.csv', sep=',')
# reverting dataframe, in order to have date with a increasing temporal order, 
# from the oldest to the most recent.
btc = btc.iloc[::-1]
btc.reset_index(drop=True, inplace=True)

# we don't want the Date feature
btc = btc.iloc[:, 3:]

# adding some technical analysis indicator
btc['SMA'] = ta.SMA(btc['Close'], timeperiod=10)
btc['WMA'] = ta.WMA(btc['Close'], timeperiod=10)
btc['RSI'] = ta.RSI(btc['Close'], timeperiod=10)
btc['ROCP'] = ta.ROCP(btc['Close'], timeperiod=10)
btc['MOM'] = ta.MOM(btc['Close'], timeperiod=10)
btc['OBV'] = ta.OBV(btc['Close'], btc['Volume'])

# dropping nan
btc = btc.dropna(axis=0)
btc.reset_index(drop=True, inplace=True)

# creating csv file
btc.to_csv(os.path.join(this_dir, 'data/hourly_data.csv'), index=False)

# saving data in mysql server
# opening configuration file for connection
with open('dbconf/private.json', 'r') as f:
    dbconf = json.load(f)

engine = create_engine('mysql+pymysql://{}:{}@localhost/{}'.format(dbconf['user'], dbconf['password'], dbconf['database']))

btc.to_sql(con=engine, name='TacHourlyData', if_exists='replace', index=False)
