#
# N . Uras - 21/10/2019
# Main file for cross validation
#

import json
import argparse
from grid_search_module import GridSearch
from kfold_cross_validation import KfoldCrossValidation

# list of actions available
actions = ['cross_validation', 'grid_search']
# list of models available
models = ['cnn', 'svm', 'xgb']     
# initializing parser
parser = argparse.ArgumentParser()  
# Adding arguments with informations about the program functionality
# The --version argument does not require a value to be given on the command 
# line. That's why we set the action argument to "store_true".
parser.add_argument(
        "-V", "--version", help='Shows the program version', action='store_true')
# parser for relative paths (n_args --> one or more than one arguments will be passed)
# all the arguments will be inserted in a list 
# if we want to pass int instead of strings we can use the type argument: type=int
parser.add_argument("-a", "--action", help='Kind of procedure to be executed', required=True, choices=actions)
parser.add_argument("-dbconf", "--dbconf", help='database configuration file path', required=True, action='store')
parser.add_argument("-modelconf", "--modelconf", help='model configuration file path', required=True, action='store')
#parser.add_argument("-f", "--frequency", help='Frequency of data price', required=False, action='store')
parser.add_argument("-m", "--model", help='Model to be executed', required=False, choices=models)
# read arguments from the command line
args = parser.parse_args()  

# opening model and database configuration files
with open(args.dbconf, 'r') as f:
    dbconf = json.load(f)

with open(args.modelconf, 'r') as f:
    model_conf = json.load(f)

if __name__ == '__main__':
    
    if args.action == 'cross_validation':
        # creating cross validation instance
        kfold_cv = KfoldCrossValidation(dbconf)
        # calling compute_cross_validation method
        # Cross validation must be modified yet as grid search, in order to pass
        # arguments through model_conf json file
        kfold_cv.compute_cross_validation(model=args.model,
                                          model_setup=model_conf['Models'][args.model],
                                          **model_conf['CrossValidation'])
    else:
        # creating grid_search instance
        grid_search = GridSearch(dbconf)
        # calling compute_grid_search method
        grid_search.compute_grid_search(model=args.model, 
                                        model_setup=model_conf['Models'][args.model]['model_caller'],
                                        **model_conf['GridSearch'][args.model])
    