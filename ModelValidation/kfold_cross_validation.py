#
# N. Uras - 21/10/2019
# K-fold Cross Validation
#


import os
import sys
import joblib
import numpy as np
import pandas as pd
from importlib import import_module
from sqlalchemy import create_engine
this_dir, _ = os.path.split(__file__)
sys.path.insert(0, this_dir.replace('ModelValidation', 'Preprocessing'))
sys.path.insert(1, this_dir.replace('ModelValidation', 'DimensionalityReduction'))
sys.path.insert(2, this_dir.replace('ModelValidation', 'Models'))
sys.path.insert(3, this_dir.replace('ModelValidation', 'FeatureSelection'))
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score


class KfoldCrossValidation():
    
    
    def __init__(self, conf):
            self.dbconf = conf
            
            
    def compute_cross_validation(self, frequency, model, k_folds, target_feature, 
                                 n_lags, n_kbest, feature_selection, model_setup, timeseries_mode):
        engine = create_engine('mysql+pymysql://{}:{}@localhost/{}'.format(
                self.dbconf['user'], self.dbconf['password'], self.dbconf['database']))
        # be careful on the selected table in the next query
        # for daily frequency data: TacDailyData
        # for hourly frequency data: TacData
        if frequency == 'hourly': 
            table = 'TacHourlyData'
        else:
            table = 'TacDailyData'
        df = pd.read_sql('SELECT * FROM {}'.format(table), con=engine)
        preprocessing = getattr(import_module('preprocessing_module'), 'PreprocessingClass')(df)
        df_scaled = preprocessing.scaler()
        # questo qui non ha senso, non è utile, perché sto validando. E' utile 
        # solamente quando scelto il miglior modello, dopo un processo di CV e GridS, 
        # e faccio un train e test reale e verifico se i risultati ottenuti rientrano nei range stimati con la CV.
#        train, test = preprocessing.train_test_split(df_scaled, percentage=0.9)
        train_cv = pd.DataFrame(df_scaled, columns=df.columns)
        if timeseries_mode == 'restricted':
            train_cv = train_cv.iloc[:, :5]
        else:
            if timeseries_mode != 'unrestricted':
                raise Exception('{} not valid input for timeseries_mode parameter'.format(timeseries_mode))
        # creating CV instance
        accuracies = []
        kfold_cv = KFold(n_splits=k_folds, shuffle=False)
        for train_index, test_index in kfold_cv.split(train_cv):
            mini_train, mini_test = train_cv.iloc[train_index], train_cv.iloc[test_index]
            mini_train.reset_index(inplace=True, drop=True)
            mini_test.reset_index(inplace=True, drop=True)
            X_train, y_train = preprocessing.temporal_structure(mini_train, target=target_feature, n_lags=n_lags)
            X_test, y_test = preprocessing.temporal_structure(mini_test, target=target_feature, n_lags=n_lags)
            # encoding results - not needed since we deal with a multiclass problem
            # targets must be one hot encoded when dealing with a multi-label classification problem
            # y_train, y_test = preprocessing.encoder(y_train, y_test)
            if model not in ['cnn']:
#                y_train = np.array([np.argmin(y) for y in y_train])
#                y_test = np.array([np.argmin(y) for y in y_test])
                # If the model is not a neural network, we make some feature selection
                if feature_selection:
                    kbest = getattr(import_module('kbest_module'), 'SelectKBestClass')(X_train, y_train, X_test)
                    X_train, X_test = kbest.compute_k_best(n_components=n_kbest)
                    # dimensionality reduction
                    pca = getattr(import_module('pca_module'), 'PCAClass')(X_train, X_test)
                    X_train, X_test, _ = pca.compute_pca()
            else:
                # dimensionality reduction
                pca = getattr(import_module('pca_module'), 'PCAClass')(X_train, X_test)
                X_train, X_test, _ = pca.compute_pca()
                tensor_depth = 1 
                # reshaping data into tensor shape: (heigth, width, depth)
                X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], tensor_depth))
                X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], tensor_depth))
            # creating model instance
            model_instance = getattr(import_module('{}_module'.format(
                    model)), '{}'.format(model.upper()))(X_train, y_train, X_test, y_test)
            # training model
            print('training model...')
            # model instantiation
            called_model = model_instance.model_caller(model_setup['model_caller'])
            # training model
            trained_model = model_instance.train(called_model, model_setup['train'])
            # making predictions
            print('making predictions...')
            y_preds = model_instance.predict(trained_model)
            if model == 'cnn':
                # translating y_test and y_preds in 1-dimensional array
#                y_test = [np.argmax(y) for y in y_test]
                # from continuos targets to binary outputs
                y_preds = y_preds.round()
            accuracies.append(accuracy_score(y_test, y_preds, normalize=True)*100)
        print('Model: {}, accuracy: {}, sigma: {}'.format(
                model, np.mean(accuracies), np.std(accuracies)))
        val_scores = {'acc': {'mean': np.mean(accuracies), 'std': np.std(accuracies)}}
        joblib.dump(val_scores, os.path.join(this_dir, 'cv_results/val_scores_{}_{}.pkl'.format(model, n_lags)))
