#
# N. Uras 7 November 2020
# Test file of gridsearch_keras python module
#

import numpy as np
import pandas as pd
from keras.utils import np_utils
from my_gridsearch import GridSearch
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler

# load dataset
df = pd.read_csv('../data/hourly_data.csv')

# split into train and test
split_index = int(len(df)*0.8)
train = df.iloc[:split_index+1, :]
test = df.iloc[split_index+1:, :]

# scale data
scaler = MinMaxScaler()
scaled_train = scaler.fit_transform(train)
scaled_test = scaler.transform(test)

# create inputs and outputs
df_scaled_train = pd.DataFrame(scaled_train, columns=df.columns)
df_scaled_test = pd.DataFrame(scaled_test, columns=df.columns)

def temporal_structure(train_or_test, target, n_lags):
    y = []
    # creating inputs
    X = pd.DataFrame()
    for col in train_or_test.columns:
        for i in range(n_lags, 0, -1):
            X['{}_t-{}'.format(col, i)] = train_or_test[col].shift(i)
    X.dropna(inplace=True)
    for i in range(0, len(train_or_test)-n_lags):
        # creating target values
        y_i = train_or_test.loc[i+n_lags, target]
        last_close = train_or_test.loc[i+n_lags-1, target]
        next_close = y_i
        # creating target values
        if last_close < next_close:
            y_i = 1    # up
        elif last_close == next_close:
            y_i = 0    # stability
        else:
            y_i = -1    # down
        # saving target value into y
        y.append(y_i)
    
    return np.array(X), np.array(y)

X_train, y_train = temporal_structure(df_scaled_train, target='Close', n_lags=2)
X_test, y_test = temporal_structure(df_scaled_test, target='Close', n_lags=2)

# encoding outputs
# label encoding
label_encoder = LabelEncoder()
y_train = label_encoder.fit_transform(y_train)
y_test = label_encoder.transform(y_test)
# one hot encoding
y_train = np_utils.to_categorical(y_train)
y_test = np_utils.to_categorical(y_test)

# reshape X as lstm expected inputs
X_train_reshaped = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
X_test_reshaped = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

# creating GridSearchKeras instance
hyperparams = {'neurons': [4, 16, 32, 64], 'epochs': [50, 100, 300], 'batch_size': [32, 64, 128]}
gd_search = GridSearch(classifier='lstm',
                       param_grid=hyperparams,
                       cv_nfolds=10,
                       classification_type='multiclass')

# calling fit method 
gd_results = gd_search.fit(X_train_reshaped, y_train)
