#
# N. Uras - 6 November 2020
# This python module includes the GridSearchKeras class that provides a useful
# tool for tuning keras deep learing model hyperparameters.
#


import os
import sys
import itertools
import numpy as np
from importlib import import_module
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
this_dir, _ = os.path.split(__file__)
sys.path.insert(0, this_dir.replace('ModelValidation', 'Models'))


class GridSearch(object):
    
    
    def __init__(self, classifier, param_grid, cv_nfolds, classification_type):
        self.classifier = classifier
        self.param_grid = param_grid
        self.cv_nfolds = cv_nfolds
        if classification_type == 'binary':
            self.average = 'binary'
        elif classification_type in ['multiclass']:
            self.average = 'weighted'
        else:
            raise TypeError(f'{classification_type} not available')
            print('Choose from [binary, multiclass]')
        
        
    def fit(self, X, y):
        # unpacking param_grid dictionary
        keys = sorted(self.param_grid)
        combinations = list(itertools.product(*(self.param_grid[hyperparam] for hyperparam in keys)))
        hyperparams_list = []
        for comb in combinations:
            d = {keys[i]: comb[i] for i in range(len(comb))}
            hyperparams_list.append(d)
        results = []
        i = 0
        for dict_ in hyperparams_list:
            i += 1
            accuracies, precisions, recalls, f1_scores = [], [], [], []
            print(f'Hyperparameters configuration {dict_} \n')
            # next step is to generate kfolds
            kfold_cv = KFold(n_splits=self.cv_nfolds, shuffle=False)
            for train_idx, test_idx in kfold_cv.split(X):
                X_train, y_train = X[train_idx], y[train_idx]
                X_test, y_test = X[test_idx], y[test_idx]
                # calling the model
                classifier = getattr(import_module(f'{self.classifier}_module'), 
                                f'{self.classifier.upper()}')(X_train, y_train, X_test, y_test)
                try:
                    trained_model = classifier.fit_model(**dict_)
                except TypeError:
                    trained_model = classifier.fit_model(dict_)
                print('Making predictions... \n')
                y_preds = classifier.predict(trained_model)
                if self.classifier in ['cnn', 'lstm']:
                    if y_preds.shape[1] > 1:
                        y_test = np.argmax(y_test, axis=-1)
                        y_preds = np.argmax(y_preds, axis=-1)
                    else:
                        # binary classification
                        y_preds = np.round(y_preds)
                # saving partial results
                accuracies.append(accuracy_score(y_test, y_preds))
                precisions.append(precision_score(y_test, y_preds, average=self.average))
                recalls.append(recall_score(y_test, y_preds, average=self.average))
                f1_scores.append(f1_score(y_test, y_preds, average=self.average))
            # saving final results
            results.append({'hyperparams': dict_, 
                            'accuracy': (np.mean(accuracies), np.std(accuracies)), 
                            'precision': (np.mean(precisions), np.std(precisions)), 
                            'recall': (np.mean(recalls), np.std(recalls)),
                            'f1_score': (np.mean(f1_scores), np.std(f1_scores))})
            print(f'Hyperparameters configuration n. {i+1}: CONCLUDED \n')
        return results