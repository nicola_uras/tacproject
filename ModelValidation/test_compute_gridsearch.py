#
# N. Uras - 11/02/2020
# grid_search_module test file.
#

import json
from compute_gridsearch import ComputeGridSearch

# opening dbconf file
conf_path = '/Users/nicolauras/Documents/MLprojects/tacproject/dbconf/private.json'
with open(conf_path, 'r') as f:
    dbconf = json.load(f)

# opening model conf file
model_path = '/Users/nicolauras/Documents/MLprojects/tacproject/model_conf.json'
with open(model_path, 'r') as f:
    model_conf = json.load(f)

gs = ComputeGridSearch(dbconf)

model = 'xgb'
results = gs.grid_search_method(model=model,
                                model_setup=model_conf['Models'][model]['model_caller'],
                                **model_conf['GridSearch'][model])
