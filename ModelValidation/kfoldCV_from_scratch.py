#
# N. Uras - 07/10/2019
# Model validation. In this script we implemente the cross validation method.
#


import pandas as pd


class KfoldCrossValidation():
    
    
    def __init__(self, train_set):
        self.train_set = train_set
        
        
    def compute_cross_validation(self, n_folds):
        """This method compute a k-fold cross validation.
        
        **********************************************************************
        
        Params:
            n_folds: number of folds to split the train set (int).
        Returns:
            cv_list: list of train and test datasets created from the 
            train_set (list).
        """
        n_data_to_cut = self.train_set.shape[0]%n_folds
        cut = int(self.train_set.shape[0] - n_data_to_cut)
        # attenzione che la funzione loc di pandas include sia l'indice inferiore che
        # quello superiore, quindi in questo modo il nostro dataset è più lungo 
        # della lunghezza voluta di un'unità! Perché va da zero sino all'indice desiderato incluso.
        self.train_set = self.train_set.loc[:cut, :]
        step = int((self.train_set.shape[0])/(n_folds))
        mini_test_indeces = [(i, i+step) for i in range(0, len(self.train_set)-step, step)]
        # creating train and test pairs
        cv_list = []
        for tpl_index in range(0, len(mini_test_indeces)):
            inf_test = mini_test_indeces[tpl_index][0]
            sup_test = mini_test_indeces[tpl_index][1]
            if tpl_index == 0:
                mini_test = self.train_set.loc[:sup_test-1, :]
                mini_train = self.train_set.loc[sup_test:, :]
                cv_list.append((mini_test, mini_train))
            elif mini_test_indeces[tpl_index] == mini_test_indeces[-1]:
                mini_test = self.train_set.loc[inf_test+1:sup_test, :]
                mini_train = self.train_set.loc[:inf_test, :]
                cv_list.append((mini_test, mini_train))
            else:
                mini_test = self.train_set.loc[inf_test:sup_test-1, :]
                mini_train_1 = self.train_set.loc[:inf_test-1, :]
                mini_train_2 = self.train_set.loc[sup_test:, :]
                mini_train = pd.concat([mini_train_1, mini_train_2])
                cv_list.append((mini_test, mini_train))
        
        return cv_list
