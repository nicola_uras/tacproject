#
# N. Uras - 25/10/2019
# Grid Search with cross validation module
#


import os
import sys
import joblib
import numpy as np
import pandas as pd
from importlib import import_module
from sqlalchemy import create_engine
from my_gridsearch import GridSearch
from keras.wrappers.scikit_learn import KerasClassifier
this_dir, _ = os.path.split(__file__)
sys.path.insert(0, this_dir.replace('ModelValidation', 'Preprocessing'))
sys.path.insert(1, this_dir.replace('ModelValidation', 'DimensionalityReduction'))
sys.path.insert(2, this_dir.replace('ModelValidation', 'Models'))
sys.path.insert(3, this_dir.replace('ModelValidation', 'FeatureSelection'))


class ComputeGridSearch():
    
    
    def __init__(self, dbconf):
        self.dbconf = dbconf
        
        
    def grid_search_method(self, frequency, target_feature, n_lags, model, params, n_folds, metric, 
                           n_kbest, feature_selection, model_setup, timeseries_mode, n_jobs, classification_type):
        engine = create_engine('mysql+pymysql://{}:{}@localhost/{}'.format(
        self.dbconf['user'], self.dbconf['password'], self.dbconf['database']))
        # be careful on the selected table in the next query
        # for daily frequency data: TacDailyData
        # for hourly frequency data: TacData
        if frequency == 'hourly': 
            table = 'TacHourlyData'
        else:
            table = 'TacDailyData'
        df = pd.read_sql('SELECT * FROM {}'.format(table), con=engine)
        if timeseries_mode == 'restricted':
            df = df.iloc[:, :5]
        else:
            if timeseries_mode != 'unrestricted':
                raise Exception('{} not valid input for timeseries_mode parameter'.format(timeseries_mode))
        preprocessing = getattr(import_module('preprocessing_module'), 'PreprocessingClass')(df)
        df_scaled = preprocessing.scaler()
        train, test = preprocessing.train_test_split(df_scaled, percentage=0.8)
        X_train, y_train = preprocessing.temporal_structure(train, target=target_feature, n_lags=n_lags)
        X_test, y_test = preprocessing.temporal_structure(test, target=target_feature, n_lags=n_lags)
        # encoding results when classification type is multiclass/multilabel
        if model in ['cnn', 'lstm']:
            pca = getattr(import_module('pca_module'), 'PCAClass')(X_train, X_test)
            X_train, X_test, _ = pca.compute_pca()
            n_features = 1 #model_setup['n_features']
            X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], n_features))
            X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], n_features))
            if classification_type in ['multiclass']:
                y_train, y_test = preprocessing.encoder(y_train, y_test)
        else:
            if feature_selection:
                kbest = getattr(import_module('kbest_module'), 'SelectKBestClass')(X_train, y_train, X_test)
                X_train, X_test = kbest.compute_k_best(n_components=n_kbest)
            # pca does not depend on feature selection
            pca = getattr(import_module('pca_module'), 'PCAClass')(X_train, X_test)
            X_train, X_test, _ = pca.compute_pca()
        # calling the model
        classifier = getattr(import_module(f'{model}_module'), f'{model.upper()}')(
            X_train, y_train, X_test, y_test)
        if model in ['cnn', 'lstm']:
            classifier = KerasClassifier(build_fn=classifier.model_caller, verbose=1)
            # instantiating 
        else:
            classifier = classifier.model_caller(model_setup)
        # instantiating grid search
        grid_search = GridSearch(classifier=model,
                                 param_grid=params,
                                 cv_nfolds=n_folds,
                                 classification_type=classification_type)
        gd_results = grid_search.fit(X_train, y_train)
        joblib.dump(gd_results, os.path.join(this_dir, f'gd_results/{model}_results_{n_lags}.pkl'))
        return gd_results
