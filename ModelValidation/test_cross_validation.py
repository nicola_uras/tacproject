#
# N. Uras - 07/10/2019
# kfold_cv_module test file. 
#

import json
from kfold_cross_validation import KfoldCrossValidation

# opening dbconf file
conf_path = '/Users/nicolauras/Documents/MLprojects/tacproject/dbconf/private.json'
with open(conf_path, 'r') as f:
    dbconf = json.load(f)

# opening model_conf file
model_path = '/Users/nicolauras/Documents/MLprojects/tacproject/model_conf.json'
with open(model_path, 'r') as g:
    model_conf = json.load(g)
   
cv_instance = KfoldCrossValidation(dbconf)

# calling compute_cross_validation method
model = 'cnn'
cv_instance.compute_cross_validation(model=model,
                                     model_setup=model_conf['Models'][model],
                                     **model_conf['CrossValidation'])
