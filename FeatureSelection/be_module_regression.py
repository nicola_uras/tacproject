#
# N. Uras - 17/10/2019
# Backward Elimination class.
#


import numpy as np
import statsmodels.formula.api as sm


class BackwardElimination():
    
    
    def __init__(self, X_train, y_train, X_test):
        self.X_train = X_train
        self.y_train = y_train
        self.X_test = X_test
        self.X_opt = list(np.arange(0, self.X_train.shape[1]))
        
        
    def compute_backward_elimination(self, alpha=0.05):
        # alpha: significance level, usually 0.05
        regressor = sm.OLS(self.y_train, self.X_train[:, self.X_opt]).fit()
        p_values = regressor.pvalues
        if all([pv < alpha for pv in p_values]):
            return self.X_train[:, self.X_opt], self.X_test[:, self.X_opt]
        else:
            exceding_pvs = [pv for pv in p_values if pv >= alpha]
            self.X_opt = [i for i, pv in enumerate(p_values) if pv != max(exceding_pvs)]
            # calling method recursively
            # Se sotto non mettessi il return, il return di sopra passerebbe 
            # il risultato alla funzione di sotto, però la funzione di sotto 
            # lo terrebbe per se e basta, non lo restituisce all'esterno, perchè
            # non c'è il return!
            return self.compute_backward_elimination()
