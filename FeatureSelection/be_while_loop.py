#
# N. Uras - 17/10/2019
# Backward elimination without recursive function but with while loop
#

# Backward elimination with while loop

import numpy as np
import statsmodels.formula.api as sm

def backward_elimination(X_train, y_train, X_test, alpha=0.05):
    
    p_values = [5]
    check = True
    X_opt = list(np.arange(0, X_train.shape[1]))
    while check:
        if all([i < alpha for i in p_values]):
            check = False
        else:
            regressor = sm.OLS(y_train, X_train[:, X_opt]).fit()
            p_values = regressor.pvalues
            exceding_pvs = [pv for pv in p_values if pv >= alpha]
            X_opt = [i for i, pv in enumerate(p_values) if pv != max(exceding_pvs)]
    
    return X_train[:, X_opt], X_test[:, X_opt]

# altra soluzione sempre con while loop
def backward_elimination_2(X_train, y_train, X_test, alpha=0.05):
    
    check = True
    X_opt = list(np.arange(0, X_train.shape[1]))
    regressor = sm.OLS(y_train, X_train[:, X_opt]).fit()
    p_values = regressor.pvalues
    while check:
        if all([i < alpha for i in p_values]):
            check = False
        else:
            exceding_pvs = [pv for pv in p_values if pv >= alpha]
            X_opt = [i for i, pv in enumerate(p_values) if pv != max(exceding_pvs)]
            regressor = sm.OLS(y_train, X_train[:, X_opt]).fit()
            p_values = regressor.pvalues
    
    return X_train[:, X_opt], X_test[:, X_opt]
        
    
    