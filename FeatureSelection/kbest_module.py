#
# N. Uras - 22/10/2019
# Select K best of sklearn library module
#


from sklearn.feature_selection import mutual_info_classif
from sklearn.feature_selection import SelectKBest


class SelectKBestClass():
    
    
    def __init__(self, X_train, y_train, X_test):
        self.X_train, self.y_train = X_train, y_train
        self.X_test = X_test
        
    def compute_k_best(self, n_components):
        kbest_instance = SelectKBest(score_func=mutual_info_classif, k=n_components)
        kbest = kbest_instance.fit(self.X_train, self.y_train)
        X_train_best, X_test_best = kbest.transform(self.X_train), kbest.transform(self.X_test)
        print(kbest.scores_)
        
        return X_train_best, X_test_best
