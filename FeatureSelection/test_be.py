#
# N. Uras - 17/10/2019
# Test file for Backward Elimination class
#

import os
import sys
import pandas as pd
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('FeatureSelection', 'data')
data_path = os.path.join(data_dir, 'data.csv')
# modifying sys to find PreprocessingClass
sys.path.insert(0, this_dir.replace('FeatureSelection', 'Preprocessing'))
sys.path.insert(1, this_dir.replace('FeatureSelection', 'DimensionalityReduction'))
from preprocessing_module import PreprocessingClass
from pca_module import PCAClass
from be_module import BackwardElimination

# opening dataset to test
df = pd.read_csv(data_path)

# creating Preprocessing class instance
prep = PreprocessingClass(df)

# testing scaler method
df_scaled = prep.scaler()

# testing train_test_split method
train, test = prep.train_test_split(df_scaled, percentage=0.8)

# creating X_train and X_test
X_train, y_train = prep.temporal_structure(train, 'Close', 3)

# creating X_test and y_test
X_test, y_test = prep.temporal_structure(test, 'Close', 3)

# creating PCA class instance
pca = PCAClass(X_train, X_test)

# testing compute_pca method
X_train_fitt, X_test_fitt, pca_scores = pca.compute_pca()

# creating BackwardElimination instance
be = BackwardElimination(X_train_fitt, y_train, X_test_fitt)

# testing compute_backward_elimination method
X_train_opt = be.compute_backward_elimination()
