#
# N. Uras - 04/10/2019
# Preprocessing class. This script aims to eventually clean the data, rescale 
# data and build the inputs and the outputs of our machile learning models.
#


import os
import joblib
import numpy as np
import pandas as pd
from keras.utils import np_utils
this_dir, _ = os.path.split(__file__)
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler


class PreprocessingClass():
    """Preprocessing TAC class.
    
    **************************************************************************
    
    attributes: 
        data: the pandas dataframe of data taken from the mysql server db.
    """

    
    def __init__(self, data):
        self.df = data
        self.columns = list(self.df.columns)

        
    def scaler(self, X_train, X_test):
        standard_scaler = StandardScaler()
        X_train_scaled = standard_scaler.fit_transform(X_train)
        X_test_scaled = standard_scaler.transform(X_test)
        joblib.dump(standard_scaler, os.path.join(this_dir, 'StandardScaler/scaler.pkl'))
        
        return X_train_scaled, X_test_scaled

    
    def train_test_split(self, dataset, percentage):
        length = dataset.shape[0]
        split = int(length * percentage)
        test = dataset[split:]
        test.reset_index(inplace=True, drop=True)
        train = dataset[:split]
    
        return train, test
    
    
    def create_target(self, dataset):
        dataset['diff'] = dataset['close'] - dataset['open']
        dataset['class'] = 1
        dataset.loc[dataset['diff'] <= 0, 'class'] = 0
        # dropping diff column
        dataset.drop('diff', inplace=True, axis=1)

        return dataset
    
    
    def temporal_structure(self, dataset, n_lag, n_forecast):
        # creating target variable
        dataset = self.create_target(dataset)
        X, y = pd.DataFrame(), pd.DataFrame()
        # creating output
        for i in range(0, n_forecast):
            if i == 0:
                y['class'] = dataset['class'].shift(-i)
            else:
                y[f'class_t+{i}'] = dataset['class'].shift(-i)
        # creating inputs
        for col in dataset.columns:
            for i in range(n_lag, 0, -1):
                X[f'{col}_t-{i}'] = dataset[col].shift(i)
        # aggregating inputs and outputs
        df_agg = pd.concat([X, y], axis=1)
        # dropping nans
        df_agg.dropna(inplace=True)
        # resetting indeces
        df_agg.reset_index(inplace=True, drop=True)
        
        return df_agg

    
    def multiclass_temporal_structure(self, train_or_test, target, n_lags, threshold):
        y = []
        # creating inputs
        X = pd.DataFrame()
        for col in train_or_test.columns:
            for i in range(n_lags, 0, -1):
                X['{}_t-{}'.format(col, i)] = train_or_test[col].shift(i)
        X.dropna(inplace=True)
        # creating target values. In this particular way beacause this is a classification problem
        # Particularly, it is a multi-class classification problem (not to be confused with multi-label 
        # classification problem) so we can set -1 for down movement, 0 for stability and 1 for rising price.
        for i in range(0, len(train_or_test)-n_lags):
            # creating target values
            y_i = train_or_test.loc[i+n_lags, target]
            
            last_close = train_or_test.loc[i+n_lags-1, target]
            next_close = y_i
            # creating target values
            if np.abs((last_close - next_close)) >= threshold:
                if (last_close - next_close) < 0:
                    y_i = 1    # up
                elif (last_close - next_close) > 0:
                    y_i = -1    # down
            else:
                y_i = 0    # stability
            # saving target value into y
            y.append(y_i)
        
        return np.array(X), np.array(y)


    def encoder(self, y_train, y_test):
        label_encoder = LabelEncoder()
        y_train = label_encoder.fit_transform(y_train)
        y_test = label_encoder.transform(y_test)
        # one hot encoding
        y_train = np_utils.to_categorical(y_train)
        y_test = np_utils.to_categorical(y_test)
        
        return y_train, y_test


    # Be careful, this function doesn't change or shuffle the temporal order inside
    # each sequences, but shuffles the sequences. This is useful in order to avoid 
    # the overfitting problem, in such a way that the model doesn't fit too much on
    # the training set and in such a way that all the sequences are independent.
    # This method must be used only within cross-validation.
    def shuffle_in_unison(a, b):
        
        assert len(a) == len(b)
        shuffled_a = np.empty(a.shape, dtype=a.dtype)
        shuffled_b = np.empty(b.shape, dtype=b.dtype)
        permutation = np.random.permutation(len(a))
        for old_index, new_index in enumerate(permutation):
            shuffled_a[new_index] = a[old_index]
            shuffled_b[new_index] = b[old_index]
        
        # shuffled_a will be X_train, while shuffled_b will be y_train
        return shuffled_a, shuffled_b
