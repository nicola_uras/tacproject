#
# N. Uras - 04/10/2019
# Test preprocessing file.
#

import os
import pandas as pd
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('Preprocessing', 'data')
data_path = os.path.join(data_dir, 'btc_hourly_data.csv')
from preprocessing_module import PreprocessingClass

# opening dataset to test
df = pd.read_csv(data_path)

df = df.iloc[:, 2:]

# creating Preprocessing class instance
prep = PreprocessingClass(df)

# testing train_test_split method
train, test = prep.train_test_split(df, percentage=0.8)

# # testing temporal_structure method
threshold = (df['Close'].min())/100
X_train, y_train = prep.temporal_structure(train, target='Close', n_lags=1, threshold=threshold)
X_test, y_test = prep.temporal_structure(test, target='Close', n_lags=1, threshold=threshold)

# testing scaler method
X_train_scaled, X_test_scaled = prep.scaler(X_train, X_test)

# # testing encoder method
y_train, y_test = prep.encoder(y_train, y_test)
