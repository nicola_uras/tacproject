#
# N. Uras - 27 October 2020
# LSTM model
#


import os
import joblib
import pandas as pd
this_dir, _ = os.path.split(__file__)
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM as lstm
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.layers import Dense


class LSTM():
    
    
    def __init__(self, model_conf):
        self.model_conf = model_conf
        self.model = Sequential()
        # se voglio mettere un solo strato LSTM devo impostare return_sequences a False
        # in modo tale che non restituisca più sequenze ma che venga compattato in un unico vettore.
        self.model.add(lstm(self.model_conf['neurons'], 
                            activation='relu', 
                            input_shape=self.model_conf['input_shape']))
        self.model.add(Dense(self.model_conf['output_dim'], 
                             activation=self.model_conf['activation']))
        self.model.summary()
        # with multiclass classification problem softmax as activation and categorical_crossentropy as loss
        # with multilabel classification problem sigmoid as activation and binary_crossentropy as loss
        self.model.compile(loss=self.model_conf['loss'], optimizer=self.model_conf['optimizer'], metrics=['accuracy'])
        
    
#     def train(self, model, train_setup):
#         early_stop = EarlyStopping(monitor='val_loss', patience=10)
#         # fitting the created model
#         model.fit(self.X_train, 
#                   self.y_train, 
#                   validation_data=(self.X_test, self.y_test),
#                   callbacks=[early_stop],
#                   **train_setup)
# #            # saving model
# #            joblib.dump(model, os.path.join(this_dir, 'saved_models/lstm.pkl'))
#         return model
    
    
    def fit_model(self, X_train, y_train, X_test, y_test):
        early_stop = EarlyStopping(monitor='val_loss', patience=20)
        # fitting model
        self.model.fit(X_train, y_train,
                       validation_data=(X_test, y_test),
                       callbacks=[early_stop],
                       epochs=self.model_conf['epochs'], 
                       batch_size=self.model_conf['batch_size'], 
                       verbose=0)
        losses = pd.DataFrame(self.model.history.history)
        self.model.save(this_dir.replace('Models', 'Updates/trained_models/lstm.h5'))
        joblib.dump(losses, this_dir.replace('Models', 'Updates/trained_models/lstm_losses.pkl'))
    
    
    def predict(self, X_test, model_path=None):
        # making predictions
        if model_path:
            saved_model = joblib.load(model_path)
            preds = saved_model.predict(X_test)
        else:
            preds = self.model.predict(X_test)
        
        return preds        
