#
# N. Uras - 21/10/2019
# Convolutional Neural Network model
#


import os
import joblib
import pandas as pd
from keras.models import Sequential
this_dir, _ = os.path.split(__file__)
from keras.layers import Conv1D, Dense
from tensorflow.keras.callbacks import EarlyStopping
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.normalization import BatchNormalization
from keras.layers.core import Dropout, Flatten


class CNN():
    
    
    def __init__(self, model_conf):
        self.model_conf = model_conf
        # creating keras model (model chosen: Convolution Neural Network)
        # Important: input_shape = (length of a sequence, tensor depth)
        # lenght of a sequence is the tensor width
        # So the model must know that will deal with a tensor of heigth X_train.shape[0] (questa non
        # non è tanto importante qua, perché tanto la rete lavora con le righe, quindi vuole
        # sapere quante colonne aspettarsi per ogni riga e la profondità di ogni riga, cioè se 
        # aspettarsi per ogni riga del tensore di input un vettore riga o una matrice. Tutto questo 
        # lo farà per ogni riga del tensore di input), a width X_train.shape[1] and a tensor with
        # a depth of "n". The latter is most important for images!
        # building the model
        self.model = Sequential()
        self.model.add(Conv1D(input_shape=self.model_conf['input_shape'], 
                              filters=self.model_conf['filters_1'], 
                              kernel_size=4, 
                              padding='same',
                              activation='relu'))
        self.model.add(BatchNormalization())
        self.model.add(LeakyReLU())
        self.model.add(Dropout(0.5))
        self.model.add(Conv1D(filters=self.model_conf['filters_2'], 
                              kernel_size=4, 
                              padding='same', 
                              activation='relu'))
        self.model.add(BatchNormalization())
        self.model.add(LeakyReLU())
        self.model.add(Dropout(0.5))
        self.model.add(Flatten())
        self.model.add(Dense(self.model_conf['neurons_dense']))
        self.model.add(BatchNormalization())
        self.model.add(LeakyReLU())
        self.model.add(Dense(self.model_conf['output_dim'], activation=self.model_conf['activation']))
        # compiling the model
        # multiclass classification: softmax as activation and categorical_crossentropy as loss
        # multilabel classification: sigmoid as activation and binary_crossentropy as loss
        self.model.compile(loss=self.model_conf['loss'],
                           optimizer=self.model_conf['optimizer'],
                           metrics=['accuracy'])
        
        
#     def train(self, model, train_setup):
#         early_stop = EarlyStopping(monitor='val_loss', patience=10)
#         # fitting the created model
#         model.fit(self.X_train, 
#                   self.y_train, 
#                   validation_data=(self.X_test, self.y_test),
#                   callbacks=[early_stop],
#                   **train_setup)
# #            # saving model
# #            joblib.dump(model, os.path.join(this_dir, 'saved_models/cnn.pkl'))
#         return model
    
    
    def fit_model(self, X_train, y_train, X_test, y_test):
        # fitting model
        early_stop = EarlyStopping(monitor='val_loss', patience=20)
        # fitting model
        self.model.fit(X_train, y_train,
                       validation_data=(X_test, y_test),
                       callbacks=[early_stop],
                       epochs=self.model_conf['epochs'], 
                       batch_size=self.model_conf['batch_size'], 
                       verbose=0)
        losses = pd.DataFrame(self.model.history.history)
        self.model.save(this_dir.replace('Models', 'Updates/trained_models/cnn.h5'))
        joblib.dump(losses, this_dir.replace('Models', 'Updates/trained_models/cnn_losses.pkl'))
    
    
    def predict(self, X_test, model_path=None):
        # making predictions
        if model_path:
            saved_model = joblib.load(model_path)
            preds = saved_model.predict(X_test)
        else:
            preds = self.model.predict(X_test)
        
        return preds
