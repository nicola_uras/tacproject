#
# N. Uras - 21/10/2019
# Support Vector Machine model
#


import os
import joblib
from sklearn.svm import SVC
this_dir, _ = os.path.split(__file__)

class SVM():
    
    
    def __init__(self, X_train, y_train, X_test, y_test):
        self.X_train, self.y_train = X_train, y_train
        self.X_test, self.y_test = X_test, y_test
       
        
    def model_caller(self, model_setup):
        model = SVC(**model_setup)
        return model
    
        
    def train(self, model, train_setup):
        model.fit(self.X_train, self.y_train)
        # saving model
#        joblib.dump(model, os.path.join(this_dir, 'saved_models/svm.pkl'))
        
        return model
    

    def fit_model(self, model_setup):
        # model_setup will a dict with hyperparameters configuration, in my_gridsearch is dict_
        model_instance = self.model_caller(model_setup)
        model_instance.fit(self.X_train, self.y_train)
        return model_instance
    
    
    def predict(self, trained_model, model_path=None):
        # making predictions
        if model_path:
            saved_model = joblib.load(model_path)
            preds = saved_model.predict(self.X_test)
        else:
            preds = trained_model.predict(self.X_test)
        
        return preds
