#
# N. Uras - 27 Jan 2021
# This module aims to aggregate all the individual parts to make predictions
# and get results
#


import os
import sys
import numpy as np
from importlib import import_module
this_dir, _ = os.path.split(__file__)
sys.path.insert(0, this_dir.replace('Updates/ml_folder', 'Preprocessing'))
sys.path.insert(1, this_dir.replace('Updates/ml_folder', 'Models'))
from preprocessing_module import PreprocessingClass
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score


class ComputeMlFunction(object):
    
    
    def __init__(self, df_conf, test_size, threshold, lag, model, model_conf):
        self.df_conf = df_conf
        self.test_size = test_size
        self.threshold = threshold
        self.lag = lag
        self.model = model
        self.model_conf = model_conf
    
    
    def __call__(self, df):
        results = {}
        self.df = df
        for key in self.df_conf:
            sup = self.df_conf[key]
            # selecting subdataset
            df = self.df.iloc[:, :sup]
            # creating Preprocessing class instance
            prep = PreprocessingClass(df)
            # split into train and test
            train, test = prep.train_test_split(df, percentage=self.test_size)
            # create temporal strcture in time series
            X_train, y_train = prep.temporal_structure(train, target='close', n_lags=self.lag, threshold=self.threshold)
            X_test, y_test = prep.temporal_structure(test, target='close', n_lags=self.lag, threshold=self.threshold)
            # testing scaler method
            X_train, X_test = prep.scaler(X_train, X_test)
            # encoding labels
            y_train, y_test = prep.encoder(y_train, y_test)
            # reshape X as lstm and cnn expected inputs
            X_train_reshaped = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
            X_test_reshaped = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
            # create model instance
            # adding fundamental key to model_conf
            self.model_conf['input_shape'] = (X_train_reshaped.shape[1], 1)
            model = getattr(import_module(
                f'{self.model}_module'), f'{self.model.upper()}')(self.model_conf)
            # training model
            model.fit_model(X_train_reshaped, y_train, X_test_reshaped, y_test)
            # making predictions
            y_preds = model.predict(X_test_reshaped)
            # from one hot encoded to 1d array - questo trasforma in un vettore unidimensionale
            # y_test = np.argmax(y_test, axis=-1)
            # y_preds = np.argmax(y_preds, axis=-1)
            y_preds = y_preds.round() # questo lascia una matrice (nella forma di one-hot-encoding)
            # saving results
            results[key] = {'accuracy': accuracy_score(y_test, y_preds),
                            'precision': precision_score(y_test, y_preds, average='weighted'),
                            'recall': recall_score(y_test, y_preds, average='weighted'),
                            'f1-score': f1_score(y_test, y_preds, average='weighted')}
            
        return results
