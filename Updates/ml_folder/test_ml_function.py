#
# N. Uras - 27 Jan 2021
# Test file of ml_function python module
#

import os
import json
import joblib
this_dir, _ = os.path.split(__file__)
data_dir = this_dir.replace('Updates/ml_folder', 'data')
import pandas as pd
from ml_function import ComputeMlFunction

# open dataset
crypto = 'btc'
df = pd.read_csv(os.path.join(data_dir, f'{crypto}_hourly_stock_affect_data.csv'))

df = df.head(2000)

# create ComputeMlFunction instance
df_conf = {'tecniche': 5,
            'trading': 56,
            'github': 64,
            'reddit_tech': 72,
            'reddit_trad': 80}
# open lstm model_conf
model = 'cnn'
with open(f'../models_conf/{model}_model_conf.json') as f:
    model_conf = json.load(f)
# define threshold
threshold = (df['close'].min())/100

mlf_instance = ComputeMlFunction(df_conf=df_conf,
                                  test_size=0.8,
                                  threshold=threshold,
                                  lag=1,
                                  model=model,
                                  model_conf=model_conf)

# calling mlf_instance
results = mlf_instance(df)

# saving results
joblib.dump(results, os.path.join(this_dir, f'results/{crypto}_errors.pkl'))
