#
# N. Uras - 12/02/2020
# Script to check balance/imbalance within classes distribution
#

import os
import sys
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib.legend_handler import HandlerBase
from matplotlib.text import Text
from matplotlib import pyplot as plt
this_dir, _ = os.path.split(__file__)
sys.path.insert(1, this_dir.replace('DataInsights', 'Preprocessing'))
from preprocessing_module import PreprocessingClass
data_path = this_dir.replace('DataInsights', 'data')

# opening dataset
data = pd.read_csv(os.path.join(data_path, 'btc_hourly_stock_affect_data.csv'))

# creating X and y arrays
prep = PreprocessingClass(data)
df = prep.temporal_structure(data, n_lag=1, n_forecast=1)

# creating class to handle new labels
class TextHandler(HandlerBase):
    def create_artists(self, legend, tup ,xdescent, ydescent,
                        width, height, fontsize,trans):
        tx = Text(width/2.,height/2,tup[0], fontsize=fontsize,
                  ha="center", va="center", color=tup[1], fontweight="bold")
        return [tx]

# generating countplot
sns.set(font_scale=1.5)
ax = sns.countplot(x='class', data=df)
# adding labels to seaborn countplot
handltext = ["-1", "0", "1"]
labels = ["Downwards", "Stability", "Upwards"]

t = ax.get_xticklabels()
labeldic = dict(zip(handltext, labels))
labels = [labeldic[h.get_text()]  for h in t]
handles = [(h.get_text(),c.get_fc()) for h,c in zip(t,ax.patches)]

ax.legend(handles, labels, handler_map={tuple : TextHandler()}) 
plt.show()

# checking number of instances per class
df['class'].value_counts()

# checking how many zeros fall in each annual bin
bin_1 = len(df.iloc[:8640][df['class'] == 0])
bin_2 = len(df.iloc[8640:17280][df['class'] == 0])
bin_3 = len(df.iloc[17280:25920][df['class'] == 0])
bin_4 = len(df.iloc[25920:][df['class'] == 0])

# from multiclass to binary classification
df_binary = prep.temporal_structure(data, n_lag=1, n_forecast=1)

sns.countplot(x=df_binary)

# seaborn distplot for bins
df_bins = pd.DataFrame(np.arange(0, 2504), columns=['bins'])
df_bins.iloc[:1838] = '2015-2016'
df_bins.iloc[1838:2383] = '2016-2017'
df_bins.iloc[2383:2449] = '2017-2018'
df_bins.iloc[2449:2504] = '2018-2019'
sns.displot(df_bins, x='bins', shrink=.7)

# check on bitcoin daily dataset
daily_data = pd.read_csv(os.path.join(data_path, 'btc_daily.csv'))
daily_data = daily_data.iloc[352:1809]
daily_data.reset_index(inplace=True, drop=True)

df_daily = prep.temporal_structure(daily_data, n_lag=1, n_forecast=1)
df_daily['class'].value_counts()
